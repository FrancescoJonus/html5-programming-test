/// <reference path="../../tsDefinitions/phaser.comments.d.ts" />

class Entity extends Phaser.Sprite {
	
	// I use this solution to identify game assets
	// It's not the only solution available, but this seems to work fine for small projects
	// However, it's probably not the best solution
	// using game.load.atlas('NAME', 'PATH/NAME.png', 'PATH/NAME.json'); is maybe the best solution, using only a file of sprite sheets
	
    static ASSETS_IMG_URL: string = "assets/img/";
    static PNG_EXT: string = ".png";

    private _name: string;

    constructor(game: Phaser.Game, x: number, y: number, key?: string | Phaser.RenderTexture | Phaser.BitmapData | PIXI.Texture, frame?: string | number) {

        super(game, x, y, key, frame);

        this._name = <string>key;
    }

    public get name(): string {

        return this._name;
    }

    public static preload(game?: Phaser.Game, name?: string): void {

        game.load.image(name, Entity.ASSETS_IMG_URL + name + Entity.PNG_EXT);
    }

    public create(newX?: number, newY?: number): Entity {

        if (newX !== undefined) {
            this.x = newX;
        }

        if (newY !== undefined) {
            this.y = newY;
        }

        return this.game.add.existing(this);
    }

    public update(): void {

    }
}