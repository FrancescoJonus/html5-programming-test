/// <reference path="../tsDefinitions/phaser.comments.d.ts" />
/// <reference path="./data/elements/Ball.ts" />
/// <reference path="./data/elements/Paddle.ts" />
/// <reference path="./data/elements/Brick.ts" />

/// <reference path="./data/BricksGroup.ts" />

// I set reference paths above the rest of the logic (also, when needed, in other files)

// The "main" file of the project, it use every (not Entity) other class introduced in the project

class Game {
	
	// As said, the class game contain the main logic of the game

    private _game: Phaser.Game;

    private _ball: Ball;
    private _paddle: Paddle;

    // private _brick: Brick; // not used, introduced for a solution that wasn't the best
    private _brickGroup: BricksGroup;
	
	private var _lives: number = 3;
	private var _livesText: string = '';
	private var _stateText: string = '';
	private var _score: number = 0;
	private var _scoreText: string = '';
	private var _scoreString: string = 'Score : ';
	private var screenDims;
	
	 // game orientation
    static correctOrientation: boolean = false;
	
	// I wanted to introduce a button's group to replace the simple "click to restart" text now displayed: I focused on insert some new other features
	// private _button: Phaser.Button;

    constructor() {
		
		// _game is created here
		// Phaser.AUTO enable the the project to auto detect the right renderer to use
		// I set "parent" as null
		// This object consist of several Phaser.State functions (all of them are explained in the Phaser's docs)
		
		// I'm calling static method calculateScreenMetrics() in ScreenUtils class which is inside Utils module 
		// This method takes default resolution for game as well as enum value which says whether the game will run in landscape or portraitThe
		// The returned object contains lot of information
		
		// I also tried to insert portrait/landscape playable mode, but, I inserted the code and applied a solution, but cannot tested it
		
		screenDims = Utils.ScreenUtils.calculateScreenMetrics(480, 320, Utils.Orientation.LANDSCAPE);

        this._game = new Phaser.Game(screenDims.gameWidth, screenDims.gameHeight, Phaser.AUTO, null, {
            init: this.init,
            preload: this.preload,
            create: this.create,
            update: this.update,
            render: this.render
        });
            
		
    }

    protected init = () => {
		
		// I'm using Phaser's USER_SCALE method for scaling game to window.
		// Also, I use here scaleX and scaleY values we previously calculated
		
		this.input.maxPointers = 1;
        this.stage.disableVisibilityChange = false;

        var screenDims = Utils.ScreenUtils.screenMetrics;

        if (this.game.device.desktop) {
            // console.log("DESKTOP"); only debugging
            this.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
            this.scale.setUserScale(screenDims.scaleX, screenDims.scaleY);
            this.scale.pageAlignHorizontally = true;
            this.scale.pageAlignVertically = true;
        }
        else {
				// console.log("MOBILE"); only debugging
                this.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
                this.scale.setUserScale(screenDims.scaleX, screenDims.scaleY);
                this.scale.pageAlignHorizontally = true;
                this.scale.pageAlignVertically = true;
                this.scale.forceOrientation(true, false);
        }
		
		// The code below was used previous to create a portrait/landscape system calculation
		// this._game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

		// I tested three different solution to stretch and fill the screen, It should be tested on various device; web testing and tools are not always reliable
        // this._game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
		
		// The background is setted to be a plain color (to debugging purpose), but there is also the code to set a background image
		// this._game.load.image('starfield', 'assets/img/starfield.jpg');
		this._game.stage.backgroundColor = '#EEE';
		
    }

    protected preload = () => {
		
		// Here I should use this to load your game assets (or those needed for the current State)
		// I command the classes that use game assets to preload

        Ball.preload(this._game);
        Paddle.preload(this._game);
        Brick.preload(this._game);
    }

    protected create = () => {

		// Here I prepare all the elements needed to start playing the game
	
        this.setPhysics();
        // this.setScale(); usable instead of the landscape/portrait system

        this.createBall();
        this.createPaddle();
        this.createBricks();
		
		// I show the score counter
		this._scoreText = this._game.add.text(10, 10, this._scoreString + this._score, { font: '34px Arial', fill: '#fff' });
		
		//  I show the lives counter
		this._livesText = this._game.add.text(this._game.world.width - 100, 10, 'Lives : ' + this._lives, { font: '34px Arial', fill: '#fff' });
		
		// If the ball goes "OutOfBounds" (as bounds are the world setted bounds), I call ballLost (to handle consequences)
		this._ball.events.onOutOfBounds.add(ballLost, this);
		
    }

    private setPhysics(): void {
		
		// I use Phaser ARCADE: this contains Arcade Physics related collision, overlap and motion methods
		// Also, I command to do not check collison for "down" orientation of screen and game (to enable the ball to go out of world's bounds)

        this._game.physics.startSystem(Phaser.Physics.ARCADE);
        this._game.physics.arcade.checkCollision.down = false;
    }

    private setScale(): void {
		
		// I align the game into the page

        this._game.scale.pageAlignHorizontally = true;
        this._game.scale.pageAlignVertically = true;
    }

    private createBall(): void {
		
		// Here I create the "ball"

        var ballX: number = this._game.world.width * 0.5;
        var ballY: number = this._game.world.height - 25;

        this._ball = new Ball(this._game, ballX, ballY);
        this._ball.create();
		
		// Then, I check when player lose all the lives
		
		if (this._lives.countLiving() < 1)
			{
				
				// I kill the objects that need to be "restarted"
				
				// game.world.removeAll(); // probably not the best solution: destroy could be better, as the references are set to null, so the garbage collector can clean everything up
				this._ball.kill();
				this._paddle.kill();
				this._brickGroup.kill();
				
				// var bricksToDestroy.filter(function(_brick) { return _brick.x <= 800; }).callAll('destroy'); // a code like this could be used to select and destroy single brick (must be modified)

				// I toggle and make visible the "game over - restart" text
				
				this._stateText.text=" GAME OVER \n Click to restart";
				this._stateText.visible = true;

				// the "click to restart" handler
				this._game.input.onTap.addOnce(this.showRestart, this);
			}
        
    }

    private createPaddle(): void {
		
		// Here I call to create the paddle of the game and specify where it must be created

        var paddleX: number = this._game.world.width * 0.5;
        var paddleY: number = this._game.world.height - 5;

        this._paddle = new Paddle(this._game, paddleX, paddleY);
        this._paddle.create();
    }

    private createBricks(): void {
		
		// I call to create a new BricksGroup (and it manage the logic to create several bricks)

        this._brickGroup = new BricksGroup(this._game);
        this._brickGroup.create();
    }

    protected update = () => {
		
		// I check for collisions between paddle, bricks and ball
		// For every collision between ball and a brick, I call to kill the brick and update score and score shown to player

        this._ball.update();
        this._paddle.update();

        this._game.physics.arcade.collide(this._ball, this._paddle);

        this._game.physics.arcade.collide(this._ball, this._paddle);
		
        if (this._game.physics.arcade.collide(this._ball, this._brickGroup) {
			this._score += 20;
			this._scoreText.text = this._scoreString + this._score;
			this._brickGroup.ballHitBrick;
		}

        this._paddle.x = this._game.input.x || this._game.world.width * 0.5;
    }

    protected render = () => {

    }

    private showRestart = () => {
		
		// here I restat game
		
		// reset the life count
		this._lives.callAll('revive');
		// reset the score counter
		this._score = 0;
		
		// creating ball again
		this.createBall();
		// creating paddle again
        this.createPaddle();
        // creating bricks again
		this.createBricks();
		
		//hides the text of game over
		this._stateText.visible = false;
		
		// Old code to simply end game
		//alert('Game over!');
		//window.location.reload();
    }
	
	private ballLost = () => {
		
		// When a ball is "lost" (goes out of setted world's bounds) I eliminate a single life to the count, update the number of lives that are shown and check if it is game over and I need to ask the player to restart it

		this._ball.kill();
		this._lives--;
		this._livesText.text = 'lives: ' + this._lives;

		if (this._lives === 0)
		{
			showRestart();
		}
		else
		{
			// creating ball again
			this.createBall();
			// this._ball.reset(this._paddle.body.x + 38, this._paddle.y - 5); // alternative method, but need different code: creating again ball is better

		}

	}
}

window.onload = () => {
	
	// here I load the project when the window is succesfully loaded

    var game = new Game();
}